'use strict';
const heml = require('heml');
const Parser = require('./parser');

let p = new Parser(heml);
module.exports.parse = (event, context, callback) => {
  try {
    let jbody = JSON.parse(event.body);
    p.parse(jbody.heml, jbody.options, callback);
  } catch (err) {
    callback(null, {
      statusCode: 400,
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      body: JSON.stringify({
        message: 'oh no something went wrong with your HEML parse',
        error: err.message // can't stringify err obj got to get message from it
        // input: event
      })
    });
  }
};
