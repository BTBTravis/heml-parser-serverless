# HEML Serverless Parser #

A serverless wrapper for the awesome HEML library

### Whats HEML? ###

"HEML is an open source markup language for building responsive email."
- https://github.com/SparkPost/heml

* [Learn HEML](https://heml.io/)
* [HEML on Github](https://github.com/SparkPost/heml)

### Setup ###

1. Clone the reop
2. Make sure you have your aws credentials https://serverless.com/framework/docs/providers/aws/guide/credentials/
3. Run `npm install`
4. Run `serverless deploy`
5. Start making request. Serverless will output an enpoint just make POST request to that with HEML in a json body under the key 'heml' key also remember to include a `x-api-key` header with one of the keys serverless exports

### Hacking on the project ###

* Install mocha globally via `npm install -g mocha` then run npm test to get started

### License ###

* [MIT](https://bitbucket.org/BTBTravis/heml-parser-serverless/raw/7ec8f1f2926ed6ed206aefea214b12b9a823aefb/LICENSE)