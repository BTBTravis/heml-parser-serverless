class Parser {
  constructor (heml) {
    this.heml = heml;
  }
  parse (rawHeml, options, callback) {
    this.heml(rawHeml, options).then(function (result) {
      callback(null, {
        statusCode: 200,
        headers: {
          'Access-Control-Allow-Origin': '*'
        },
        body: JSON.stringify({
          message: 'enjoy your HEML yo',
          result: result
          // input: event
        })
      });
    }).catch(function (err) {
      // console.log({err: err});
      callback(null, {
        statusCode: 400,
        headers: {
          'Access-Control-Allow-Origin': '*'
        },
        body: JSON.stringify({
          message: 'oh no something went wrong with your HEML parse',
          error: err.message // can't stringify err obj got to get message from it
          // input: event
        })
      });
    });
  }
}

module.exports = Parser;
