var assert = require('chai').assert;
var should = require('chai').should();
var handler = require('./handler');

describe ('Parser', function () {
  describe('Safely fail on malform inputs', function () {
    it('handle missing heml', function (done) {
      let event = {
        body: '{"notheml":"<p>Hello World</p>"}'
      };
      let callback = function (err, responce) {
        // console.log({responce: responce});
        let body = JSON.parse(responce.body);
        body.error.should.be.a('string');
        responce.statusCode.should.equal(400);
        done();
      };
      handler.parse(event, null, callback);
    });
    it('handle missing body', function (done) {
      let event = {
        notbody: '{"heml":"<p>Hello World</p>"}'
      };
      let callback = function (err, responce) {
        // console.log({responce: responce});
        let body = JSON.parse(responce.body);
        body.error.should.be.a('string');
        responce.statusCode.should.equal(400);
        done();
      };
      handler.parse(event, null, callback);
    });
  });

  describe('Simple <p> tag parse', function () {
    it('should add add classes to p tag', function (done) {
      let event = {
        body: '{"heml":"<p>Hello World</p>"}'
      };
      let callback = function (err, responce) {
        // console.log(responce);
        let body = JSON.parse(responce.body);
        body.result.errors.should.be.a('array');
        body.result.errors.should.have.lengthOf(0);
        body.result.html.should.equal('<p class="text p">Hello World</p>');
        responce.statusCode.should.equal(200);
        done();
      };
      handler.parse(event, null, callback);
    });
    it('safe fail', function (done) {
      let event = {
        body: '{"heml":"<heml><p>Strict Hello World</p></heml>","options":{"validate":"strict"}}'
      };
      let callback = function (err, responce) {
        let body = JSON.parse(responce.body);
        body.error.should.be.a('string');
        responce.statusCode.should.equal(400);
        done();
      };
      handler.parse(event, null, callback);
    });
  });
});
